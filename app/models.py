from django.db import models
from django.contrib.auth.models import User

class QuestionManager(models.Manager): 
    def best_question(self, output_count):
        return self.order_by('-questionlikes__count_likes')[0: output_count]

    def new_question(self, output_count):
        return self.order_by("-date_publication")[0: output_count].select_related()

    def get_one_question(self, pk):
        return Question.objects.get(pk=pk)   

class TagManager(models.Manager): 
    def get_one_tag(self, pk):
        return Tag.objects.get(pk=pk)  

    def get_questions_by_tag(self, tag):
        return tag.question_set.all()             

class Tag(models.Model):
    title = models.CharField(max_length=255, unique=True)
    objects = TagManager()

    def __str__(self): 
        return self.title

    class Meta:
        verbose_name = 'Тег'
        verbose_name_plural = 'Теги'

class Profile(models.Model):
    nick_name = models.CharField(max_length=255)
    email = models.EmailField(unique=True)
    avatar = models.ImageField(upload_to='./media/images', blank=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.nick_name

    class Meta:
        verbose_name = 'Аккаунт'
        verbose_name_plural = 'Аккаунты'    

class Question(models.Model):
    title = models.CharField(max_length=255)
    text = models.TextField()
    date_publication = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(Profile, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag)

    objects = QuestionManager()

    def __str__(self): 
        return self.title

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'    

class Answer(models.Model):
    text = models.TextField()  
    correct = models.BooleanField()
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    author = models.ForeignKey(Profile, on_delete=models.CASCADE)

    def __str__(self): 
        return self.text

    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'

class QuestionLikes(models.Model):
  user = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True)
  question = models.ForeignKey(Question, on_delete=models.CASCADE, null=True)
  count_likes = models.IntegerField(default=0)

  def __str__(self):
    return self.user

  class Meta:
    verbose_name = 'Лайки вопроса'
    verbose_name_plural = 'Лайки вопроса'


class AnswerLikes(models.Model):
  user = models.ForeignKey('Profile', on_delete=models.CASCADE, null=True)
  answer = models.ForeignKey('Answer', on_delete=models.CASCADE)
  count_likes = models.IntegerField(default=0)

  def __str__(self):
    return self.user

  class Meta:
    verbose_name = 'Лайки ответа'
    verbose_name_plural = 'Лайки ответа'





