from django.contrib import admin

# Register your models here.

from app.models import Answer, Question, Profile, Tag, AnswerLikes, QuestionLikes

admin.site.register(Answer)
admin.site.register(Question)
admin.site.register(Profile)
admin.site.register(Tag)
admin.site.register(AnswerLikes)
admin.site.register(QuestionLikes)