from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from app.models import Question, Answer, Tag, Profile

MAX_COUNT_POST_ON_PAGE = 3
MAX_COUNT_ANSWER_ON_PAGE = 5
SIZE_OF_TOP_QUESTION = 15
DEFAULT_COUNT_POST_ON_PAGE = 10
COUNT_NEW_QUESTIONS_TO_OUTPUT = 15

def index(request):
    new_questions = Question.objects.new_question(Question.objects.count())
    page, posts = paginate(new_questions, request, MAX_COUNT_POST_ON_PAGE)
    return render(request,  
	          'index.html',  
		  {'page': page,  
		   'posts': posts}) 

def ask(request): 
    return render(request, 'ask.html', {})

def login(request): 
    return render(request, 'login.html', {})

def question(request, pk): 
    question = Question.objects.get_one_question(pk)
    answers = question.answer_set.all()
    page, posts = paginate(answers, request, MAX_COUNT_ANSWER_ON_PAGE)
    return render(request, 
                 'question.html', 
                 {'question': question,
                 'page': page,
                 'posts': posts})

def questions_tags(request, pk): 

    tag = Tag.objects.get_one_tag(pk)
    questions = Tag.objects.get_questions_by_tag(tag)
    page, posts = paginate(questions, request, 3) 
    return render(request,  
	          'questions-tags.html',  
		      {'tag': tag,
              'page': page,  
		      'posts': posts})

def settings(request): 
    return render(request, 'settings.html', {})

def signup(request): 
    return render(request, 'signup.html', {})

def hot(request): 
    hot_questions = Question.objects.best_question(Question.objects.count())
    page, posts = paginate(hot_questions, request, MAX_COUNT_POST_ON_PAGE)
    return render(request,  
	          'hot.html',  
		  {'page': page,  
		   'posts': posts})  

def paginate(objects_list, request, per_page=DEFAULT_COUNT_POST_ON_PAGE):
    paginator = Paginator(objects_list, per_page) 
    page = request.GET.get('page') 
    try:  
        posts = paginator.page(page)  
    except PageNotAnInteger:  
        posts = paginator.page(1)  
    except EmptyPage:  
        posts = paginator.page(paginator.num_pages)  
    return page, posts
