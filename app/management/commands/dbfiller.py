
from django.core.management.base import BaseCommand
from faker import Faker
from app.models import Question
from app.models import Answer
from django.contrib.auth.models import User
from app.models import Profile
from app.models import AnswerLikes
from app.models import QuestionLikes
from app.models import Tag
from datetime import datetime
import random

COUNT_TAGS = 10000
COUNT_QUESTIONS = 100000
COUNT_USERS = 10000
COUNT_ANSWERS = 1000000
COUNT_LIKES = 1000000
LENGTH_TITLES = 128
LENGTH_TEXT = 200

fake = Faker()                       

class Command(BaseCommand):
  def add_arguments(self, parser):
    for i in range(COUNT_TAGS):
        Tag.objects.create(title = "tag" + str(i))
    for i in range(COUNT_USERS):  
        user = User.objects.create(username = "user" + str(i), 
                                  password = fake.password())
        Profile.objects.create(nick_name = user.username,
                              email = "user" + str(i) + "@gmail.com",
                              user = user)
    for i in range(COUNT_QUESTIONS):
        question = Question.objects.create(title = fake.sentence()[:LENGTH_TITLES],
                                        text = fake.sentence()[:LENGTH_TEXT],
                                        date_publication = datetime.now(),
                                        author_id = random.randint(2, COUNT_USERS - 1))
        question.tags.add(Tag.objects.get(pk=random.randint(1, COUNT_TAGS - 1)))
    for i in range(COUNT_ANSWERS):
        answer = Answer.objects.create(text = fake.sentence()[:LENGTH_TEXT],
                                    correct = False,
                                    question_id = random.randint(1, COUNT_QUESTIONS - 1),
                                    author_id = random.randint(2, COUNT_USERS - 1))
    for i in range(COUNT_QUESTIONS):
        answer = Answer.objects.create(text = fake.sentence()[:LENGTH_TEXT],
                                    correct = False,
                                    question_id = i + 1,
                                    author_id = random.randint(2, COUNT_USERS - 1))     
    for i in range(COUNT_LIKES):
        QuestionLikes.objects.create(count_likes = random.randint(1, COUNT_USERS),
                                    user_id = random.randint(2, COUNT_USERS - 1),
                                    question_id = random.randint(1, COUNT_QUESTIONS - 1))
        AnswerLikes.objects.create(count_likes = random.randint(1, COUNT_USERS),
                                    user_id = random.randint(2, COUNT_USERS - 1),
                                    answer_id = random.randint(0, COUNT_ANSWERS - 1)) 
